{
  description = "kd flake";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/master;
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, home-manager,... }:
    let
      lib = nixpkgs.lib;
      pkgs = nixpkgs.legacyPackages/${system};
      system = "x86_64-linux";
    in {
    nixosConfigurations = {
      foxGo = lib.nixosSystem {
        inherit = system;
        modules = [ ./configuration.nix ];
	  };
  };

    homeConfigurations = {
      kd = home-manager.lib.homeManagerConfiguration {
        inherit = pkgs;
        modules = [ ./home.nix ];
	    };
    };
  };

}